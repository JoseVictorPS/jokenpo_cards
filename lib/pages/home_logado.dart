import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:jokenpo_cards/globals.dart' as globals;

class HomeLogado extends StatefulWidget {

  @override
  _HomeLogadoState createState() => _HomeLogadoState();
}

class _HomeLogadoState extends State<HomeLogado> {

  var currentUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
              'Olá, ${currentUser.displayName}',
              style: TextStyle(
                  fontSize: 24,
                  letterSpacing: 2,
                  fontFamily: 'Pangolin'
              )
          ),
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset('assets/images/jokenpo.png'),
                Text(
                    'JoKenPô Cards',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/multi_player');
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*3/4,
                      height: MediaQuery.of(context).size.height*1/13,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            'Multijogador',
                            style: TextStyle(
                                fontSize: 36,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      ),
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/single_player');
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*3/4,
                      height: MediaQuery.of(context).size.height*1/13,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            'Jogador Único',
                            style: TextStyle(
                                fontSize: 36,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      ),
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/history');
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*3/4,
                      height: MediaQuery.of(context).size.height*1/13,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            'Histórico',
                            style: TextStyle(
                                fontSize: 36,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      ),
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      logOut();
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*1/2,
                      height: MediaQuery.of(context).size.height*1/13,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            'Logout',
                            style: TextStyle(
                                fontSize: 36,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      ),
                    )
                ),
                SizedBox(height: MediaQuery.of(context).size.height*1/10),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void logOut(){
    FirebaseAuth.instance.signOut();
    globals.loginLock = false;
  }

}
