import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flame/gestures.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
// import 'package:get/get.dart';
import 'package:jokenpo_cards/classes/match.dart';
import 'package:jokenpo_cards/classes/player.dart';

import 'package:jokenpo_cards/components/background.dart';
import 'package:jokenpo_cards/components/carta.dart';
import 'package:jokenpo_cards/components/eCard.dart';
import 'package:jokenpo_cards/components/score-display.dart';

class MyGame extends StatefulWidget {
  final Match match;

  const MyGame({Key key, this.match}) : super(key: key);

  @override
  _MyGameState createState() => _MyGameState();
}

class _MyGameState extends State<MyGame> {
  List<Carta> cards = [];

  final String route =
      FirebaseAuth.instance.currentUser != null ? "/home_logado" : "/home";
  @override
  Widget build(BuildContext context) {
    Match match = widget.match;
    TestGame game = TestGame.withMatch(widget.match);
    var myGame = game.widget;

    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: StreamBuilder(
                stream: widget.match.db.onValue,
                // ignore: missing_return
                builder: (context, AsyncSnapshot<Event> snapshot) { //stream builder pegaria as mudanças do db em tempo real
                  if (snapshot.hasData) {
                    DataSnapshot dataValues = snapshot.data.snapshot;
                    Map<dynamic, dynamic> values = dataValues.value;
                    // match = updateMatch(match, values);
                    // game.updateMatch(values);
                  }
                  return Text('Olá!');
                })),
        body: myGame);
  }
}

enum GameState { STARTED, ENDED }

class TestGame extends Game with TapDetector {
  Size screenSize; // tamanho da tela
  double tileSizeX; // largura da carta
  double tileSizeY; // altura da carta
  List<Carta> cards = []; // lista de cartas
  List<double> positions; // lista das posições de cada carta
  Background background; // background
  bool cardOnGame = false; //verifica se há algum card sendo jogado
  Random rnd; // variável para a aleatoriedade das cartas
  Match match;
  int score;
  List<ScoreDisplay> scoreDisplays;
  Rect scoreRect;
  int nPlayers = 3;
  int nCards;
  User currentUser = FirebaseAuth.instance.currentUser;
  int enemyCard = 1; // 0 = Pedra, 1 = Papel, 2 = Tesoura, 3 = Aguardando;
  ECard eCard;
  List<Carta> cardsOnTable = [];
  int cardsPerPlayer = 0;
  Iterator playerOfTurn;
  Timer timer;
  Carta cardPlayed;
  Canvas canvas;
  Player previousPlayer;
  Player nextPlayer;
  List<Player> playersOfRound = [];
  GameState state;
  var subscriptionMatch;

  TestGame.withMatch(Match match) {
    this.match = match;
    nPlayers = this.match.players.length;
    initialize();
  }

  TestGame() {
    initialize();
  }

  //stream do db de match
  listenToMatch() {
    this.subscriptionMatch = this.match.db.onValue.listen((event) {
      var snapshot = event.snapshot;
      if (snapshot.value != null) this.updateMatch(snapshot.value);
    });
  }

  void initialize() async {
    this.state = GameState.STARTED;
    rnd = Random(); // inicializa a variável rnd que gera números aleatórios
    resize(await Flame.util
        .initialDimensions()); // aguarda o flame retornar as dimensões da tela
    score = 0;
    background = Background(this); // inicializa a clases do background

    this.listenToMatch();

    //!apenas o host faz a distribuição de cartas
    if (currentUser.uid != this.match.host.userId) return;
    //iterator responsável por passar pelos jogadores do turno
    this.playersOfRound.add(this.match.players.elementAt(0));
    this.playersOfRound.add(this.match.players.elementAt(1));
    this.playerOfTurn = this.playersOfRound.iterator;
    await this.moveToNextPlayer();
    spawnDisplay();
    spawnCard(); // cria um objeto carta e o adiciona na lista de cartas
  }

  void spawnDisplay() {
    scoreDisplays = [];
    scoreRect = Rect.fromLTWH(
        1, 1, screenSize.width - 1, screenSize.height * 0.03 * nPlayers);
    double posx = 0;
    double posy = 0;
    for (var i = 0; i < nPlayers; i++) {
      scoreDisplays.add(ScoreDisplay(
          this, posx, posy, this.match.players.elementAt(i).name + ": "));
      posy += screenSize.height * 0.03;
    }
  }

  // faz a distribuição de cartas
  void spawnCard() {
    positions = [
      0.0
    ]; // inicializa a lista com as posições horizontais das cartas
    int formula = (9 + ((nPlayers % 3 == 0) ? 1 : 0));
    cardsPerPlayer = formula;
    nCards = nPlayers * formula; // quantidade de cartas
    for (var i = 0; i < formula; i++) {
      positions.add(positions.last + tileSizeX + screenSize.width * 0.0125);
    }
    List<double> qtcartas = [nCards / 3, nCards / 3, nCards / 3];
    cards = [];
    //adiciona uma carta na lista de cartas para ser desenhada na tela
    for (Player player in this.match.players) {
      for (var i = 0; i < formula; i++) {
        int x = rnd.nextInt(3);
        if (qtcartas[x] > 0) {
          Carta currentCard = Carta(this, positions[i],
              screenSize.height - tileSizeY - screenSize.height / 16, x);
          qtcartas[x] -= 1;
          if (player is HumanPlayer && player.userId == currentUser.uid) {
            cards.add(currentCard);
          }
          currentCard.setPlayer = player;
          player.addCardToHand(currentCard);
          this.match.cardsInGame.add(currentCard);
        } else {
          i--;
        }
      }
    }

    this.match.saveMatch();
  }

  //definira qual jogador será o inimigo, nessa caso a sua carta ficara na parte de cima da tela
  Player setEnemy() {
    if (this.playersOfRound.any((player) => player.userId == currentUser.uid)) {
      return this
          .playersOfRound
          .singleWhere((player) => player.userId != currentUser.uid);
    } else {
      return this.playersOfRound.elementAt(1);
    }
  }

  //gerencia a rodada, por exemplo se for a vez do robo jogara a carta dele, ou se for a vez do jogador o liberará para jogar
  // penso em usar essa mesma função para gerenciar os jogadores no multiplayer também, caso for preciso
  manageTurn() async {
    if (this.cardsOnTable.length == 2) return;
    Player currentPlayer = await this.currentPlayer();
    print(currentPlayer.name);
    if (currentPlayer.userId != currentUser.uid) {
      if (currentPlayer is BotPlayer) {
        BotPlayer current = currentPlayer as BotPlayer;
        Carta chosenCard = current.playCard(this.match.cardsInGame);
        this.cardsOnTable.add(chosenCard);
        this.match.addCardPlayed(chosenCard);
        Player enemy = setEnemy();
        if (enemy == chosenCard.player)
          chosenCard.isEnemyCard();
        else
          chosenCard.isNotEnemy();
        cards.add(chosenCard);
        await this.moveToNextPlayer();
        sleep(const Duration(seconds: 1));
      }
    } else if (currentPlayer.userId == currentUser.uid) {
      cardOnGame = false;
    }
  }

  //faz o reset do jogador atual, caso já tenha passado pelo iterator todo
  Future<Player> currentPlayer() async {
    if (this.playerOfTurn.current == null) {
      this.playerOfTurn = this.match.players.iterator;
      this.playerOfTurn.moveNext();
      await this.match.updateCurrentPlayer(this.playerOfTurn.current);
      sleep(const Duration(seconds: 1));
    }
    return this.playerOfTurn.current;
  }

  Future<void> onLoad() async {
    // carrega todas as imagens para manter a fluidez do jogo
    await Flame.images.loadAll(<String>[
      'PAPER.png',
      'ROCK.png',
      'SCISSOR.png',
      'PAPER_E.png',
      'ROCK_E.png',
      'SCISSOR_E.png',
      'background.png',
    ]);
  }

  void resize(Size size) {
    // proporções da tela e tamanho do tile do retângulo da carta
    screenSize = size;
    nPlayers == 3
        ? tileSizeX = screenSize.width * 0.08875
        : tileSizeX = screenSize.width * 0.1;
    tileSizeY = screenSize.height * 0.1284;
    super.resize(size);
  }

  Future<void> update(double dt) async {

    // se o jogo acabou, será criado uma função que o terminará e criara um jogo no historico
    if (this.match.cardsInGame.length == 0) {
      print("jogo terminou");
      this.subscriptionMatch.cancel();
      if (!this.match.ongoing) this.match.endMatch();
      this.state = GameState.ENDED;
      return;
    }

    await manageCardsOnTable();

    this.manageTurn();

    //o array card tme todas as cartas que aparecem na tela
    for (var i = 0; i < this.cards.length; i++) {
      cards[i].update(dt);
    }

    for (var i = 0; i < nPlayers; i++) {
      scoreDisplays[i].update(dt);
    }
  }

  //observara se tem duas cartas na mesa e caso haver duas, limpara o array de cartas na mesa, o array (Cards) de cartas
  // na tela e contabilizara os pontos
  manageCardsOnTable() async {
    int i = 0;

    if (this.cardsOnTable.length == 2 &&
        this.playersOfRound[1].userId == currentUser.uid) {
      await Future.delayed(const Duration(seconds: 2), () {
        i = 1;
      });
    } else if (this.cardsOnTable.length == 2) {
      print("sleeping");
      i = 1;
      sleep(const Duration(seconds: 2));
    }

    if (this.cardsOnTable.length == 2 && i == 1) {
      this.cards.remove(cardsOnTable[0]);
      this.cards.remove(cardsOnTable[1]);
      this.match.cardsInGame.remove(cardsOnTable[1]);
      this.match.cardsInGame.remove(cardsOnTable[0]);
      this.cardsOnTable.clear();
      Player winnerOfRound = this.match.checkPontuation();
      int pos = this.match.players.indexOf(winnerOfRound);
      if (pos >= 0) scoreDisplays[pos].score = winnerOfRound.points;
      await this.setPlayersOfRound();
    }
  }

  //função decide quem serão os 2 jogadores do turno
  setPlayersOfRound() async {
    if (this.match.players.length > 2) {
      int pos =
          this.match.players.indexOf(this.playersOfRound.elementAt(1)) + 1;
      Player next;
      if (pos >= this.match.players.length) {
        next = this.match.players.first;
      } else {
        next = this.match.players.elementAt(pos);
      }
      this.playersOfRound.clear();
      this.playersOfRound.add(next);
      pos = this.match.players.indexOf(next) + 1;
      if (pos == this.match.players.length) pos = 0;
      this.playersOfRound.add(this.match.players.elementAt(pos));
      print(this.playersOfRound[0].name + ", " + this.playersOfRound[1].name);
    }
    this.playerOfTurn = this.playersOfRound.iterator;
    await this.moveToNextPlayer();
    sleep(const Duration(seconds: 1));
  }

  @override
  void render(Canvas canvas) {
    // desenha o fundo da tela
    background.render(canvas);

    // desenha todos os cards da lista na tela

    for (var i = 0; i < cards.length; i++) {
      cards[i].render(canvas);
    }

    Paint scorePaint = Paint();
    scorePaint.color = Color(0x9C000000);
    canvas.drawRect(scoreRect, scorePaint);

    for (var i = 0; i < nPlayers; i++) {
      scoreDisplays[i].render(canvas);
    }
  }

  // função que é ativada quando o cliente clica na tela
  // passa para o proximo jogador
  //so funciona se o jogador for o jogador atual
  Future<void> onTapDown(TapDownDetails d) async {
    if (this.match.currentPlayer.userId != this.currentUser.uid) return;
    for (var i = 0; i < this.cards.length; i++) {
      if (cards[i].cardRect.contains(d.globalPosition)) {
        if (cardOnGame) return;
        cards[i].onTapDown();
        this.cardsOnTable.add(cards[i]);
        this.match.addCardPlayed(cards[i]);
        await this.moveToNextPlayer();
      }
    }
  }

  //função que passa para o proximo jogador e atualiza o db
  moveToNextPlayer() async {
    this.playerOfTurn.moveNext();
    if (this.playerOfTurn.current != null)
      await this.match.updateCurrentPlayer(this.playerOfTurn.current);
  }

  // atualiza as classes do jogo com as novas info do banco
  updateMatch(Map json) {
    this.match.host = this
        .match
        .players
        .singleWhere((element) => element.dbKey == json['host']['dbKey']);

    for (var player in json['players']) {
      Player currentPlayer = match.players
          .singleWhere((element) => element.dbKey == player['dbKey']);
      int pos = match.players.indexOf(currentPlayer);
      this.match.players[pos].points = player['points'];
      this.match.players[pos].isPlaying = player['isPlaying'];
    }

    for (var card in json['cardsPlayed']) {
      this.updateCardsOnList(card, this.cards);
      this.updateCardsOnList(card, this.match.cardsPlayed);
      this.updateCardsOnList(card, this.cardsOnTable);
    }

    this.match.ongoing = json['ongoing'];

    if (json['currentPlayer'] != null) {
      //currentPlayer
      this.match.currentPlayer.points = json['points'];
      this.match.currentPlayer.isPlaying = json['isPlaying'];
      this.match.currentPlayer.cardsInHand = cards;
    }

    return match;
  }

  updateCardsOnList(Map card, List list) {
    int cardPosition =
        list.indexWhere((element) => element.dbKey == card['dbKey']);
    if (cardPosition > -1) {
      Carta updated = list[cardPosition];
      updated.cardRect = Rect.fromLTWH(
          card['left'], card['top'], card['width'], card['height']);
      list[cardPosition] = updated;
    } else {
      Carta newCard = new Carta(this, card['left'], card['top'], card['type']);
      newCard.setPlayer = this
          .match
          .players
          .singleWhere((element) => element.dbKey == card['player']);
      list.add(newCard);
    }
  }
}
