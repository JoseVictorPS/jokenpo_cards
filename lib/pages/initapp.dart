import 'dart:io';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/classes/room.dart';
import 'package:jokenpo_cards/pages/waiting_room.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:jokenpo_cards/globals.dart' as globals;
import 'package:http/http.dart';

class InitApp extends StatefulWidget {

  @override
  InitAppState createState() => InitAppState();

}

class InitAppState extends State<InitApp> {

  InitAppState(){
    init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(19, 96, 168, 1),
        elevation: 0,
      ),
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.width*1/8,
              ),
              Text(
                'Carregando...',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                  letterSpacing: 2,
                  fontFamily: 'Roboto',
                )
              ),
              CircularProgressIndicator(
                strokeWidth: 10,
              ),
              Container(
                color: Color.fromRGBO(19, 96, 168, 1),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height*1/7,
              )
            ]
          )
        ]
      )
    );
  }

  Future<void> init() async{
    await Firebase.initializeApp();
    OneSignal.shared.setAppId("bc542c61-5be7-4850-9193-e188efc09710");
    /*Future.delayed(Duration(seconds: 4), () async {
      var response = await sendNotification([tokenId], "Testando o One Signal", "olá");
      print(response.statusCode);
    });*/
    FirebaseAuth.instance.userChanges().listen((user) async {
      if (user != null && user.displayName != null){
        var status = await OneSignal.shared.getDeviceState();
        String tokenId = status.userId;
        FirebaseFirestore.instance.collection('usuarios').doc(user.uid).update({'token_id': tokenId});
        if(!globals.loginLock) {
          Navigator.pushNamed(context, '/home_logado');
        }
      }else{
        Navigator.pushNamed(context, '/home');
      }
    });

    OneSignal.shared.setNotificationWillShowInForegroundHandler((OSNotificationReceivedEvent event) {
      event.complete(null);
      print('Recebi notificação dentro do app');
      if(FirebaseAuth.instance.currentUser != null && globals.notificationLock == false) {
        globals.loginLock = true;
        _onInviteReceived(event.notification.additionalData['name'], event.notification.additionalData['creatorID']);
      }
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      print('Recebi notificação fora do app');
      if(FirebaseAuth.instance.currentUser != null && globals.notificationLock == false) {
        globals.loginLock = true;
        globals.navigatorKey.currentState.pushNamed('/home_logado');
        _onInviteReceived(result.notification.additionalData['name'], result.notification.additionalData['creatorID']);
      }
    });
  }

  Future<bool> _onInviteReceived(String inviterName, String hostID){
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("O usuário $inviterName está te chamando para uma partida."),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text("Recusar")
            ),
            ElevatedButton(
                onPressed: () async {
                  Room sala = await enterRoom(hostID);
                  if(sala != null){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WaitingRoom(currentRoom: sala,)
                        )
                    );
                  }
                },
                child: Text("Aceitar")
            )
          ],
        )
    );
  }

  Future<Room> enterRoom(String idAnfitriao) async {
    var updatedRoom = await FirebaseFirestore.instance.collection('saguoes')
        .doc(idAnfitriao)
        .get();
    var updatedData = updatedRoom.data();

    var user = FirebaseAuth.instance.currentUser;
    var userData = {
      'usuario': user.displayName,
      'id_usuario': user.uid,
      'pronto': false
    };
    updatedData['numero_atual_de_jogadores'] += 1;

    if (updatedData['numero_atual_de_jogadores'] <=
        updatedData['numero_maximo_de_jogadores']) {
      await FirebaseFirestore.instance.collection('saguoes')
          .doc(idAnfitriao)
          .set(updatedData);
      await FirebaseFirestore.instance.collection('saguoes').doc(idAnfitriao)
          .collection('usuarios').doc(user.uid)
          .set(userData);
      return Room(updatedData['nome'],
          updatedData['numero_maximo_de_jogadores'],
          updatedData['senha'],
          updatedData['anfitriao'],
          updatedData['id_criador']
      );
    } else {
      _showErrorMessage('A sala está lotada');
      return null;
    }
  }

  Future<bool> _showErrorMessage(String text){
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(text),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text("OK")
            ),
          ],
        )
    );
  }

}
