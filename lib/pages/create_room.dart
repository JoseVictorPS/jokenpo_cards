import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/classes/room.dart';
import 'package:jokenpo_cards/pages/waiting_room.dart';

class CreateRoom extends StatefulWidget {
  @override
  _CreateRoomState createState() => _CreateRoomState();
}

class _CreateRoomState extends State<CreateRoom> {

  final _roomNameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int dropdownValue = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset('assets/images/jokenpo.png'),
                Text(
                    'Criar Sala',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width*5/6,
                        height: MediaQuery.of(context).size.height*1/10,
                        child: TextFormField(
                          controller: _roomNameController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              icon: Icon(Icons.meeting_room_outlined),
                              border: UnderlineInputBorder(),
                              filled: true,
                              fillColor: Color.fromRGBO(204, 208, 211, 1),
                              labelText: 'Nome da Sala'
                          ),
                          validator: (value){
                            if (value == null || value.isEmpty){
                              return "Digite um nome para a sala";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width*5/6,
                        height: MediaQuery.of(context).size.height*1/10,
                        child: TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                              icon: Icon(Icons.password_rounded),
                              border: UnderlineInputBorder(),
                              filled: true,
                              fillColor: Color.fromRGBO(204, 208, 211, 1),
                              labelText: 'Senha da Sala'
                          ),
                          validator: (value){
                            if (value == null || value.length > 6){
                              return "Digite uma senha de até 6 caracteres";
                            }
                            return null;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                    'Nº de Jogadores:',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Container(
                  width: MediaQuery.of(context).size.width*2/3,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black)
                  ),
                  padding: const EdgeInsets.only(left: 20.0),
                  child: DropdownButton<int>(
                    value: dropdownValue,
                    icon: const Icon(Icons.keyboard_arrow_down_outlined),
                    iconSize: 32,
                    elevation: 16,
                    isExpanded: true,
                    style: const TextStyle(
                        fontSize: 24,
                        color: Colors.black
                    ),
                    underline: Container(
                      height: 0,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                    onChanged: (int newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: <int>[1, 2, 3, 4]
                        .map<DropdownMenuItem<int>>((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(value.toString()),
                      );
                    }).toList(),
                  ),
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()){
                        createRoom(
                            _roomNameController.text,
                            _passwordController.text,
                            dropdownValue
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WaitingRoom(
                                currentRoom: Room(
                                  _roomNameController.text,
                                  1,
                                  _passwordController.text,
                                  FirebaseAuth.instance.currentUser.uid,
                                    FirebaseAuth.instance.currentUser.uid
                                ),
                              )
                          )
                        );
                      }
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                      child: Text(
                          'Criar',
                          style: TextStyle(
                              fontSize: 36,
                              letterSpacing: 3,
                              fontFamily: 'Pangolin'
                          )
                      ),
                    )
                ),
                SizedBox(height: MediaQuery.of(context).size.height*1/10),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void createRoom(String roomName, String password, int numberOfPlayers) async{
    
    var user = FirebaseAuth.instance.currentUser;
    var data = {
      'nome': roomName,
      'senha': password,
      'numero_maximo_de_jogadores': numberOfPlayers,
      'numero_atual_de_jogadores': 1,
      'anfitriao': user.uid,
      'id_criador': user.uid
    };
    var userData = {
      'usuario': user.displayName,
      'id_usuario': user.uid,
      'pronto': true
    };

    await FirebaseFirestore.instance.collection('saguoes')
        .doc(user.uid)
        .set(data);

    await FirebaseFirestore.instance.collection('saguoes')
        .doc(user.uid)
        .collection('usuarios')
        .doc(user.uid)
        .set(userData);
  }

}
