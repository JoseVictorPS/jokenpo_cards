import 'dart:async';
import 'dart:io';
import 'package:jokenpo_cards/classes/player.dart';
import 'package:jokenpo_cards/globals.dart' as globals;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/classes/chat_msg.dart';
import 'package:jokenpo_cards/classes/room.dart';
import 'package:jokenpo_cards/classes/match.dart';
import 'package:jokenpo_cards/classes/waiting_user.dart';

import 'invite.dart';

class WaitingRoom extends StatefulWidget {

  Room currentRoom;

  WaitingRoom({Key key, @required this.currentRoom}) : super(key : key);

  @override
  _WaitingRoomState createState() => _WaitingRoomState();
}

class _WaitingRoomState extends State<WaitingRoom> {

  StreamSubscription<QuerySnapshot> _participantsSubscription;
  StreamSubscription<QuerySnapshot> _chatSubscription;
  StreamSubscription<DocumentSnapshot> _roomSubscription;

  var _participants = [];
  var _chat = [];
  final linhaLength = 25;
  final _chatController = TextEditingController();
  ScrollController _chatWindowController = new ScrollController();
  final _chatKey = GlobalKey<FormState>();
  var textoPronto = 'Pronto';
  var botaoConvidar;

  @override
  void initState() {
    super.initState();
    this._checkHost();
    this._listenToChat();
    this._listenToParticipants();
    this._listenToRoom();
    globals.loginLock = false;
    globals.notificationLock = true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Color.fromRGBO(229, 229, 229, 1),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  /*Container(
                    color: Color.fromRGBO(19, 96, 168, 1),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.03,
                  ),*/
                  Text(
                      '${widget.currentRoom.nome}',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          letterSpacing: 2,
                          fontFamily: 'Roboto'
                      )
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width*0.9,
                      height: MediaQuery.of(context).size.height*0.185,
                      decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Colors.black),
                          color: Color.fromRGBO(156, 208, 235, 1)
                      ),
                      child:  ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: _participants.length,
                        itemBuilder: (context, index) {
                          if(widget.currentRoom.hostId == FirebaseAuth.instance.currentUser.uid && FirebaseAuth.instance.currentUser.uid != _participants[index].id){
                            return Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Container(
                                  height: MediaQuery.of(context).size.height*0.045,
                                  decoration: BoxDecoration(
                                    border: Border.all(width: 2, color: Colors.black),
                                    color: Color.fromRGBO(100, 145, 233, 1),
                                  ),
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          '${_participants[index].nome}',
                                          style: TextStyle(
                                              fontSize: 24,
                                              fontFamily: 'Pangolin',
                                              color: Color.fromRGBO(235, 235, 235, 1)
                                          ),
                                        ),
                                        IconButton(
                                            onPressed: () {
                                              // Excluir jogador da sala
                                              _expulsar_jogador(_participants[index].id);
                                            },
                                            padding: EdgeInsets.all(0),
                                            alignment: Alignment.center,
                                            icon: Icon(Icons.highlight_remove, size: MediaQuery.of(context).size.height*0.043),
                                            color: Colors.red[700]
                                        )
                                      ]
                                  ),
                                )
                            );
                          }
                          else{
                            return Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Container(
                                  height: MediaQuery.of(context).size.height*0.045,
                                  decoration: BoxDecoration(
                                    border: Border.all(width: 2, color: Colors.black),
                                    color: Color.fromRGBO(100, 145, 233, 1),
                                  ),
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          '${_participants[index].nome}',
                                          style: TextStyle(
                                              fontSize: 24,
                                              fontFamily: 'Pangolin',
                                              color: Color.fromRGBO(235, 235, 235, 1)
                                          ),
                                        ),
                                      ]
                                  ),
                                )
                            );
                          }
                        },
                      )
                  ),
                  Column(
                      children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width*0.9,
                            height: MediaQuery.of(context).size.height*0.247,
                            decoration: BoxDecoration(
                                border: Border.all(width: 2, color: Colors.black),
                                color: Color.fromRGBO(235, 235, 235, 1)//Color.fromRGBO(204, 208, 211, 1)
                            ),
                            child:  ListView.builder(
                                scrollDirection: Axis.vertical,
                                controller: _chatWindowController,
                                shrinkWrap: true,
                                itemCount: _chat.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(width: 0, color: Color.fromRGBO(235, 235, 235, 1)),
                                            color: Color.fromRGBO(235, 235, 235, 1),
                                          ),
                                          child: Column(
                                              children: <Widget>[
                                                Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Container(
                                                        width: MediaQuery.of(context).size.width*0.85,
                                                        child: Text(
                                                          '${_chat[index].username}: ${_chat[index].msg}',
                                                          style: TextStyle(
                                                              fontSize: 20,
                                                              fontFamily: 'Pangolin',
                                                              color: Color.fromRGBO(0, 0, 0, 1)
                                                          ),
                                                        ),
                                                      )
                                                    ]
                                                )
                                              ]
                                          )
                                      )
                                  );
                                }
                            )
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width*0.9,
                          decoration: BoxDecoration(
                            border: Border.all(width: 2, color: Color.fromRGBO(0, 0, 0, 1)),
                          ),
                          child: Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width*0.758,
                                  height: MediaQuery.of(context).size.height*1/15,
                                  child: Form(
                                    key: _chatKey,
                                    child: TextFormField(
                                      controller: _chatController,
                                      decoration: InputDecoration(
                                          border: UnderlineInputBorder(),
                                          filled: true,
                                          fillColor: Color.fromRGBO(204, 208, 211, 1),
                                          labelText: 'Digite sua mensagem...'
                                      ),
                                      validator: (value){
                                        if(value == null || value.isEmpty){
                                          return "Por favor, digite alguma coisa antes de enviar.";
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 50,
                                  decoration: BoxDecoration(
                                    border: Border.all(width: 0, color: Color.fromRGBO(235, 235, 235, 1)),
                                    color: Color.fromRGBO(19, 96, 168, 1),
                                  ),
                                  child: IconButton(
                                    onPressed: () {
                                      if (_chatKey.currentState.validate()){
                                        _sendChatMessage(_chatController.text);
                                        _chatController.clear();
                                      }
                                    },
                                    color: Color.fromRGBO(234, 238, 231, 1),
                                    icon: Icon(Icons.send, size: 30),
                                  ),
                                ),
                              ]
                          ),
                        ),
                      ]
                  ),
                  ElevatedButton(
                      onPressed: () {
                        _changePlayerState();
                      },
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30)
                              )
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                        child: Text(
                            textoPronto,
                            style: TextStyle(
                                fontSize: 28,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      )
                  ),
                  botaoConvidar,
                  ElevatedButton(
                      onPressed: _onBackPressed,
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30)
                              )
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                        child: Text(
                            'Deixar Saguão',
                            style: TextStyle(
                                fontSize: 28,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      )
                  ),
                  Container(
                    color: Color.fromRGBO(19, 96, 168, 1),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.03,
                  )
                ],
              )
            ],
          )
      ),
    );
  }

  Future<bool> _showErrorMessage(String text){
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(text),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text("OK")
            ),
          ],
        )
    );
  }

  void _listenToParticipants(){

    this._participantsSubscription = FirebaseFirestore.instance
        .collection('saguoes')
        .doc(widget.currentRoom.creatorId)
        .collection('usuarios')
        .snapshots().listen((snapshot){
          if(snapshot.docs.isNotEmpty){
            bool meAchei = false;
            setState((){
              _participants = [];
              snapshot.docs.forEach((document) {
                if(document.data()['id_usuario'] == FirebaseAuth.instance.currentUser.uid){
                  meAchei = true;
                }
                _participants.add(WaitingUser(
                    document.data()['usuario'],
                    document.data()['id_usuario'],
                    document.data()['pronto']
                )
                );
              });
            });
            if(meAchei == false){
              _showErrorMessage("Você foi expulso da sala.");
              Navigator.pushNamed(context, '/home_logado');
            }
          }
    });
  }

  void _listenToRoom(){
    _roomSubscription = FirebaseFirestore.instance
        .collection('saguoes')
        .doc(widget.currentRoom.creatorId)
        .snapshots()
        .listen((snapshot) {
      setState(() {
        widget.currentRoom = Room(
            snapshot.data()['nome'],
            snapshot.data()['numero_maximo_de_jogadores'],
            snapshot.data()['senha'],
            snapshot.data()['anfitriao'],
            snapshot.data()['id_criador']
        );
      });
      _checkHost();
    });
  }

  void _listenToChat(){

    this._chatSubscription = FirebaseFirestore.instance
        .collection('saguoes')
        .doc(widget.currentRoom.creatorId)
        .collection('chat')
        .orderBy('hora', descending: false)
        .snapshots().listen((snapshot) {
      setState(() {
        this._chat = [];
        snapshot.docs.forEach((document) {
          _chat.add(ChatMsg(
              document.data()['mensagem'],
              document.data()['usuario']
          ));
        });
      });
      Future.delayed(Duration(milliseconds: 300), (){
        _updateScroller();
      });
    });
  }

  void _updateScroller() async{
    setState((){
      this._chatWindowController.animateTo(
          _chatWindowController.position.maxScrollExtent,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300)
      );
    });
  }

  void _sendChatMessage(String message){

    final messageData = {
      'usuario': FirebaseAuth.instance.currentUser.displayName,
      'mensagem': message,
      'hora': DateTime.now().millisecondsSinceEpoch
    };
    FirebaseFirestore.instance
        .collection('saguoes')
        .doc(widget.currentRoom.creatorId)
        .collection('chat')
        .add(messageData);

  }

  void _changePlayerState() async{
    final currentUserState = await FirebaseFirestore.instance
        .collection('saguoes')
        .doc(widget.currentRoom.creatorId)
        .collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .get();

    final data = currentUserState.data();

    data['pronto'] = !data['pronto'];

    FirebaseFirestore.instance
        .collection('saguoes')
        .doc(widget.currentRoom.creatorId)
        .collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .set(data);

  }

  Future<bool> _onBackPressed(){
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Tem certeza que deseja sair da sala ?"),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text("Não")
            ),
            ElevatedButton(
                onPressed: _exitRoom,
                child: Text("Sim")
            )
          ],
        )
    );
  }

  void _checkHost(){

    setState(() {
      if (widget.currentRoom.hostId == FirebaseAuth.instance.currentUser.uid){
        textoPronto = 'Iniciar';
        botaoConvidar = ElevatedButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Invite(currentRoom: widget.currentRoom,)
                  )
              );
            },
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)
                    )
                ),
                backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
              child: Text(
                  'Convidar',
                  style: TextStyle(
                      fontSize: 28,
                      letterSpacing: 3,
                      fontFamily: 'Pangolin'
                  )
              ),
            )
        );
      }else{
        textoPronto = 'Pronto';
        botaoConvidar = Container();
      }
    });
  }

  void _exitRoom(){

    DocumentReference room = FirebaseFirestore.instance.collection('saguoes').doc(widget.currentRoom.creatorId);
    DocumentReference user = room
        .collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser.uid);

    _participantsSubscription.cancel();
    _chatSubscription.cancel();
    _roomSubscription.cancel();

    FirebaseFirestore.instance.runTransaction((transaction) async{

      DocumentSnapshot updatedRoom = await transaction.get(room);
      String userId = FirebaseAuth.instance.currentUser.uid;

      //print(updatedRoom.data()['numero_atual_de_jogadores']);
      //print(updatedRoom.exists);
      int newNumberOfParticipants = updatedRoom.data()["numero_atual_de_jogadores"] - 1;
      //print(newNumberOfParticipants);

      if (newNumberOfParticipants == 0){
        room.collection('usuarios').get().then((snapshot){
          for (DocumentSnapshot ds in snapshot.docs){
            ds.reference.delete();
          }
        });
        room.collection('chat').get().then((snapshot){
          for (DocumentSnapshot ds in snapshot.docs){
            ds.reference.delete();
          }
        });
        room.delete();
      }else{
        if (userId == updatedRoom.data()['anfitriao']){

          String newHostId;

          for (WaitingUser participant in _participants){
            if (participant.id != FirebaseAuth.instance.currentUser.uid){
              newHostId = participant.id;
              break;
            }
          }
          transaction.update(room, {'numero_atual_de_jogadores': newNumberOfParticipants, 'anfitriao': newHostId});
        }else{
          transaction.update(room, {'numero_atual_de_jogadores': newNumberOfParticipants});
        }

        transaction.delete(user);
      }

    });
    globals.notificationLock = false;
    Navigator.pushNamed(context, '/home_logado');
  }

  void _expulsar_jogador(String kickadoId){
    DocumentReference room = FirebaseFirestore.instance.collection('saguoes').doc(widget.currentRoom.creatorId);
    DocumentReference kickadoDoc = room
        .collection('usuarios')
        .doc(kickadoId);

    FirebaseFirestore.instance.runTransaction((transaction) async{

      DocumentSnapshot updatedRoom = await transaction.get(room);

      //print(updatedRoom.data()['numero_atual_de_jogadores']);
      //print(updatedRoom.exists);
      int newNumberOfParticipants = updatedRoom.data()["numero_atual_de_jogadores"] - 1;
      //print(newNumberOfParticipants);

      transaction.update(room, {'numero_atual_de_jogadores': newNumberOfParticipants});

      transaction.delete(kickadoDoc);
    });
  }

  // fiz uma função que criaria o match, ainda não tenho 100% de certeza que está funcionando
  _createMatch(List participants, String hostId) {
    List<Player> players = [];

    for (var participant in participants) {
      HumanPlayer currentPlayer = new HumanPlayer.withUser(
          participant.id, participant.nome.trim());
      ;
      players.add(currentPlayer);
    }

    Player host =
    players.singleWhere((element) => element.userId == hostId);

    Match match = new Match(players, host, [], true);

    return match;
  }

}
