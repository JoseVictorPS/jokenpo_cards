import 'package:flutter/material.dart';

class MultiPlayer extends StatefulWidget {
  @override
  _MultiPlayerState createState() => _MultiPlayerState();
}

class _MultiPlayerState extends State<MultiPlayer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset('assets/images/jokenpo.png'),
                Text(
                    'Multi-Jogador',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/create_room');
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*3/4,
                      height: MediaQuery.of(context).size.height*1/13,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            'Criar Sala',
                            style: TextStyle(
                                fontSize: 36,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      ),
                    )
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/search_room');
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            )
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                    ),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width*3/4,
                      height: MediaQuery.of(context).size.height*1/13,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                            'Buscar Sala',
                            style: TextStyle(
                                fontSize: 36,
                                letterSpacing: 3,
                                fontFamily: 'Pangolin'
                            )
                        ),
                      ),
                    )
                ),
                SizedBox(height: MediaQuery.of(context).size.height*1/10),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }
}
