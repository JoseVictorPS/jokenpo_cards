import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/classes/room.dart';
import 'package:jokenpo_cards/pages/create_room.dart';
import 'package:jokenpo_cards/pages/waiting_room.dart';

class SearchRoom extends StatefulWidget {
  @override
  _SearchRoomState createState() => _SearchRoomState();
}

class _SearchRoomState extends State<SearchRoom> {

  final _searchController = TextEditingController();
  List<Room> roomList = [];

  @override
  void initState() {
    super.initState();
    this.searchRoom("");
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                    'Buscar Salas',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Row(
                  children: <Widget>[
                    SizedBox(
                    width: MediaQuery.of(context).size.width*4/6,
                    child: TextFormField(
                      controller: _searchController,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          filled: true,
                          fillColor: Color.fromRGBO(204, 208, 211, 1),
                          labelText: 'Nome da Sala...'
                        ),
                      ),
                    ),
                    ElevatedButton.icon(
                        onPressed: () {
                          searchRoom(_searchController.text);
                        },
                        label: Text(""),
                        icon: Icon(Icons.search, size: 35),
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          padding: EdgeInsets.fromLTRB(15, 10, 10, 10),
                        )
                    ),
                  ],
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.55,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.black),
                        color: Color.fromRGBO(156, 208, 235, 1)
                    ),
                    child:  ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: roomList.length,
                      itemBuilder: (context, index) {
                          return Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Container(
                                color: Color.fromRGBO(100, 145, 233, 1),
                                height: MediaQuery.of(context).size.height*0.09,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          bool checkPassword;
                                          if(roomList[index].password.isNotEmpty){
                                             _checkPassword(roomList[index]);
                                             checkPassword = false;
                                          }else{
                                            checkPassword = true;
                                          }
                                          if(checkPassword){
                                            bool sucess = await enterRoom(roomList[index].creatorId);
                                            if(sucess){
                                              await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => WaitingRoom(currentRoom: roomList[index],)
                                                  )
                                              );
                                              searchRoom("");
                                            }
                                          }
                                        },
                                        style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                                        ),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            '${roomList[index].nome}',
                                            style: TextStyle(
                                              fontSize: 24,
                                              fontFamily: 'Pangolin',
                                              color: Color.fromRGBO(235, 235, 235, 1)
                                            ),
                                          ),
                                        )
                                     )
                                )
                          );
                      },
                    )
                ),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void searchRoom(String roomName) async{

    var rooms = FirebaseFirestore.instance.collection('saguoes');
    QuerySnapshot roomSearch;

    if (roomName.isEmpty){
      roomSearch = await rooms.limit(20).get();
    }else{
      roomSearch = await rooms.where('nome', isEqualTo: roomName).limit(20).get();
    }

    roomList = [];

    roomSearch.docs.forEach((document) {
      setState(() {
        roomList.add(
            Room(
                document['nome'],
                document['numero_maximo_de_jogadores'],
                document['senha'],
                document['anfitriao'],
                document['id_criador']
            )
        );
      });
    });

  }

  Future<bool> _showErrorMessage(String text){
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(text),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text("OK")
            ),
          ],
        )
    );
  }

  Future<bool> enterRoom(String idAnfitriao) async{
    
    var updatedRoom = await FirebaseFirestore.instance.collection('saguoes').doc(idAnfitriao).get();
    var updatedData = updatedRoom.data();

    var user = FirebaseAuth.instance.currentUser;
    var userData = {
      'usuario': user.displayName,
      'id_usuario': user.uid,
      'pronto': false
    };
    updatedData['numero_atual_de_jogadores'] += 1;

    if (updatedData['numero_atual_de_jogadores'] <= updatedData['numero_maximo_de_jogadores']){
      await FirebaseFirestore.instance.collection('saguoes').doc(idAnfitriao).set(updatedData);
      await FirebaseFirestore.instance.collection('saguoes').doc(idAnfitriao).collection('usuarios').doc(user.uid).set(userData);
      return true;
    }else{
      _showErrorMessage('A sala está lotada');
      return false;
    }

  }

  Future<bool> _checkPassword(Room room){

    final _passwordController = TextEditingController();

    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Digite a senha da Sala"),
          content: TextFormField(
            controller: _passwordController,
            keyboardType: TextInputType.visiblePassword,
            decoration: InputDecoration(
                border: UnderlineInputBorder(),
                filled: true,
                fillColor: Color.fromRGBO(204, 208, 211, 1),
                labelText: 'Senha da sala...'
            ),
          ),
          actions: [
            ElevatedButton(
                onPressed: () async {
                  if(_passwordController.text == room.password){
                    bool sucess = await enterRoom(room.creatorId);
                    if(sucess){
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WaitingRoom(currentRoom: room,)
                          )
                      );
                      searchRoom("");
                    }
                  }else{
                    _showErrorMessage("Senha Incorreta");
                  }
                },
                child: Text("Entrar")
            ),
          ],
        )
    );

  }

}
