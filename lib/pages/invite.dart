import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:jokenpo_cards/classes/invite_user.dart';
import 'package:jokenpo_cards/classes/room.dart';

class Invite extends StatefulWidget {

  Room currentRoom;

  Invite({Key key, @required this.currentRoom}) : super(key : key);

  @override
  _InviteState createState() => _InviteState();
}

class _InviteState extends State<Invite> {

  final _searchController = TextEditingController();
  List<InviteUser> playerList = [];

  @override
  void initState() {
    super.initState();
    this.searchPlayers("");
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                    'Convidar Jogadores',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Row(
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width*4/6,
                      child: TextFormField(
                        controller: _searchController,
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            filled: true,
                            fillColor: Color.fromRGBO(204, 208, 211, 1),
                            labelText: 'Nome do jogador...'
                        ),
                      ),
                    ),
                    ElevatedButton.icon(
                        onPressed: () {
                          searchPlayers(_searchController.text);
                        },
                        label: Text(""),
                        icon: Icon(Icons.search, size: 35),
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          padding: EdgeInsets.fromLTRB(15, 10, 10, 10),
                        )
                    ),
                  ],
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.55,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.black),
                        color: Color.fromRGBO(156, 208, 235, 1)
                    ),
                    child:  ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: playerList.length,
                      itemBuilder: (context, index) {
                        return Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Container(
                                color: Color.fromRGBO(100, 145, 233, 1),
                                height: MediaQuery.of(context).size.height*0.09,
                                child: ElevatedButton(
                                    onPressed: () async {
                                      //CHANGE
                                      invitePlayer([playerList[index].oneSignalId], "O usuário ${FirebaseAuth.instance.currentUser.displayName} está te convidando pra uma partida", "JoKenPo Cards");
                                    },
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        '${playerList[index].nome}',
                                        style: TextStyle(
                                            fontSize: 24,
                                            fontFamily: 'Pangolin',
                                            color: Color.fromRGBO(235, 235, 235, 1)
                                        ),
                                      ),
                                    )
                                )
                            )
                        );
                      },
                    )
                ),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void searchPlayers(String playerName) async{

    var users = FirebaseFirestore.instance.collection('usuarios');
    QuerySnapshot usersSearch;

    if (playerName.isEmpty){
      usersSearch = await users.limit(20).get();
    }else{
      usersSearch = await users.where('nome', isEqualTo: playerName).limit(20).get();
    }

    playerList = [];

    usersSearch.docs.forEach((document) {
      setState(() {
        if(document['id_usuario'] != FirebaseAuth.instance.currentUser.uid){
          try{
            playerList.add(
                InviteUser(
                    document['nome_de_usuario'],
                    document['id_usuario'],
                    document['token_id']
                )
            );
          }catch(e){
            playerList.add(
                InviteUser(
                    document['nome_de_usuario'],
                    document['id_usuario'],
                    '1'
                )
            );
          }
        }
      });
    });

  }

  Future<bool> _showErrorMessage(String text){
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(text),
          actions: [
            ElevatedButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text("OK")
            ),
          ],
        )
    );
  }

  Future<Response> invitePlayer(List<String> tokenIdList, String contents, String heading) async{

    return await post(
      Uri.parse('https://onesignal.com/api/v1/notifications'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>
      {
        "app_id":"bc542c61-5be7-4850-9193-e188efc09710",//kAppId is the App Id that one get from the OneSignal When the application is registered.

        "include_player_ids": tokenIdList,//tokenIdList Is the List of All the Token Id to to Whom notification must be sent.

        // android_accent_color reprsent the color of the heading text in the notifiction
        "android_accent_color":"FF9976D2",

        "small_icon":"ic_stat_onesignal_default",

        "large_icon":"https://www.filepicker.io/api/file/zPloHSmnQsix82nlj9Aj?filename=name.jpg",

        "headings": {"en": heading},

        "contents": {"en": contents},

        "data": {"name": FirebaseAuth.instance.currentUser.displayName, "creatorID": widget.currentRoom.creatorId}

      }),
    );
  }

}
