import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset('assets/images/jokenpo.png'),
                Text(
                    'JoKenPô Cards',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width*5/6,
                          height: MediaQuery.of(context).size.height*1/9,
                          child: TextFormField(
                            controller: _emailController,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                                icon: Icon(Icons.email),
                                border: UnderlineInputBorder(),
                                filled: true,
                                fillColor: Color.fromRGBO(204, 208, 211, 1),
                                labelText: 'Email'
                            ),
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return "Por favor, digite seu email";
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width*5/6,
                          height: MediaQuery.of(context).size.height*1/9,
                          child: TextFormField(
                            controller: _passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                icon: Icon(Icons.password_rounded),
                                border: UnderlineInputBorder(),
                                filled: true,
                                fillColor: Color.fromRGBO(204, 208, 211, 1),
                                labelText: 'Senha'
                            ),
                            validator: (value){
                              if(value == null || value.isEmpty){
                                return "Por favor, digite sua senha";
                              }
                              return null;
                            },
                          ),
                        ),
                        ElevatedButton(
                            onPressed: () {
                              if(_formKey.currentState.validate()){
                                loginWithEmailAndPassword(
                                    context,
                                    _emailController.text,
                                    _passwordController.text
                                );
                              }
                            },
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30)
                                    )
                                ),
                                backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                            ),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                              child: Text(
                                  'Login',
                                  style: TextStyle(
                                      fontSize: 36,
                                      letterSpacing: 3,
                                      fontFamily: 'Pangolin'
                                  )
                              ),
                            )
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height*1/10),
                      ],
                    ),
                ),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void _showErrorMessage(BuildContext context, String title, Exception e) {
    showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(
            title,
            style: const TextStyle(fontSize: 24),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  '${(e as dynamic).message}',
                  style: const TextStyle(fontSize: 18),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                'OK',
                style: TextStyle(color: Colors.deepPurple),
              ),
            ),
          ],
        );
      },
    );
  }

  void loginWithEmailAndPassword(BuildContext context, String email, String password) async {

    try{
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email,
          password: password);
    } on FirebaseAuthException catch (e){
      _showErrorMessage(context, "Erro no login", e);
    }

  }

}
