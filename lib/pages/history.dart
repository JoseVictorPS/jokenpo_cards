import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/classes/match_history.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {

  var HistoryList = [];

  @override
  void initState() {
    super.initState();
    this.loadHistoric();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                    'Histórico',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height*0.65,
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, color: Colors.black),
                        color: Color.fromRGBO(156, 208, 235, 1)
                    ),
                    child:  ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: HistoryList.length,
                      itemBuilder: (context, index) {
                        return Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Container(
                                height: MediaQuery.of(context).size.height*0.235,
                                decoration: BoxDecoration(
                                    border: Border.all(width: 2, color: Colors.black),
                                    color: Color.fromRGBO(100, 145, 233, 1),
                                ),
                                child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                            Text(
                                              '${HistoryList[index].matchName}',
                                              style: TextStyle(
                                                  fontSize: 24,
                                                  fontFamily: 'Pangolin',
                                                  color: Color.fromRGBO(235, 235, 235, 1)
                                              ),
                                            ),
                                          ]
                                      ),
                                      Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              '${HistoryList[index].ifWon()[0]}',
                                              style: TextStyle(
                                                  fontSize: 24,
                                                  fontFamily: 'Pangolin',
                                                  color: HistoryList[index].ifWon()[1]
                                              ),
                                            ),
                                          ]
                                      ),
                                      Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context).size.height*0.15,
                                          decoration: BoxDecoration(
                                              border: Border.all(width: 2, color: Colors.black),
                                              color: Color.fromRGBO(100, 145, 233, 1),
                                          ),
                                          child:ListView.builder(
                                              scrollDirection: Axis.vertical,
                                              itemCount: HistoryList[index].participants.length,
                                              itemBuilder: (contextB, indexB) {
                                                return Row(
                                                    children: <Widget>[
                                                      Column(
                                                          children: <Widget>[
                                                            Text(
                                                              '  ${HistoryList[index].participants[indexB][0]}',
                                                              style: TextStyle(
                                                                  fontSize: 20,
                                                                  fontFamily: 'Pangolin',
                                                                  color: Color.fromRGBO(235, 235, 235, 1)
                                                              ),
                                                            ),
                                                          ]
                                                      ),
                                                      Column(
                                                          children: <Widget>[
                                                            Text(
                                                              '  ${HistoryList[index].participants[indexB][1]}',
                                                              style: TextStyle(
                                                                  fontSize: 18,
                                                                  fontFamily: 'Pangolin',
                                                                  color: Color.fromRGBO(235, 235, 235, 1)
                                                              ),
                                                            ),
                                                          ]
                                                      )
                                                    ]
                                                );
                                              }
                                          )
                                      ),
                                    ]
                                )
                            )
                        );
                      },
                    )
                ),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void loadHistoric() async {
    var historic = await FirebaseFirestore.instance.collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .collection('HistoricoDePartidas')
        .orderBy('timestamp', descending: true)
        .limit(15)
        .get();
    historic.docs.forEach((document) {
      var dados = document.data();
      var temp = [];
      
      for(var key in dados['participantes'].keys){
        temp.add([key, dados['participantes'][key]]);
      }
      
      setState(() {
        HistoryList.add(
            MatchHistory(
                dados['nome_da_partida'],
                dados['vitoria'],
                temp
            ));
      });
    });

  }

}
