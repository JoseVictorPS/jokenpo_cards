import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/classes/player.dart';
import 'package:jokenpo_cards/classes/match.dart';
import 'package:jokenpo_cards/pages/test_game.dart';

class SinglePlayer extends StatefulWidget {
  @override
  _SinglePlayerState createState() => _SinglePlayerState();
}

class _SinglePlayerState extends State<SinglePlayer> {
  
  Match createMatch(int amountOfBots) {
    List<Player> players = [];

    var currentUser = FirebaseAuth.instance.currentUser;

    HumanPlayer currentPlayer = currentUser == null
        ? new HumanPlayer('Jogador 1') //? haverah uma tela para insercao de nome quando eh single player e o usuario nao esta logado?
        : new HumanPlayer.withUser(currentUser.uid, currentUser.displayName);

    players.add(currentPlayer);

    for (var i = 0; i < amountOfBots; i++) {
      BotPlayer bot = new BotPlayer("Bot $i");
      players.add(bot);
    }

    Match match = new Match(players, currentPlayer, [], true);
    return match;
  }

  static int dropdownValue = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset('assets/images/jokenpo.png'),
                Text('Jogador Único',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        letterSpacing: 2,
                        fontFamily: 'Roboto')),
                Text('Nº de Bots:',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        letterSpacing: 2,
                        fontFamily: 'Roboto')),
                Container(
                  width: MediaQuery.of(context).size.width * 2 / 3,
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  padding: const EdgeInsets.only(left: 20.0),
                  child: DropdownButton<int>(
                    value: dropdownValue,
                    icon: const Icon(Icons.keyboard_arrow_down_outlined),
                    iconSize: 32,
                    elevation: 16,
                    isExpanded: true,
                    style: const TextStyle(fontSize: 24, color: Colors.black),
                    underline: Container(
                      height: 0,
                      color: Color.fromRGBO(0, 0, 0, 1),
                    ),
                    onChanged: (int newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items:
                        <int>[1, 2, 3].map<DropdownMenuItem<int>>((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(value.toString()),
                      );
                    }).toList(),
                  ),
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                MyGame(match: createMatch(dropdownValue))),
                      );
                    },
                    style: ButtonStyle(
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30))),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Color.fromRGBO(100, 145, 233, 1))),
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                      child: Text('Começar',
                          style: TextStyle(
                              fontSize: 36,
                              letterSpacing: 3,
                              fontFamily: 'Pangolin')),
                    )),
                SizedBox(height: MediaQuery.of(context).size.height * 1 / 10),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 1 / 12,
                )
              ],
            )
          ],
        ));
  }
}
