import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  final _userController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19, 96, 168, 1),
          elevation: 0,
        ),
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset('assets/images/jokenpo.png'),
                Text(
                    'JoKenPô Cards',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                        letterSpacing: 2,
                        fontFamily: 'Roboto'
                    )
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width*5/6,
                        height: MediaQuery.of(context).size.height*1/10,
                        child: TextFormField(
                          controller: _userController,
                          keyboardType: TextInputType.name,
                          decoration: InputDecoration(
                              icon: Icon(Icons.account_circle_rounded),
                              border: UnderlineInputBorder(),
                              filled: true,
                              fillColor: Color.fromRGBO(204, 208, 211, 1),
                              labelText: 'Nome de Usuário'
                          ),
                          validator: (value){
                            if(value == null || value.isEmpty){
                              return "Por favor, digite o nome do usuário";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width*5/6,
                        height: MediaQuery.of(context).size.height*1/10,
                        child: TextFormField(
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              icon: Icon(Icons.email),
                              border: UnderlineInputBorder(),
                              filled: true,
                              fillColor: Color.fromRGBO(204, 208, 211, 1),
                              labelText: 'Email'
                          ),
                          validator: (value){
                            if(value == null || value.isEmpty){
                              return "Por favor, digite seu email";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width*5/6,
                        height: MediaQuery.of(context).size.height*1/10,
                        child: TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                              icon: Icon(Icons.password_rounded),
                              border: UnderlineInputBorder(),
                              filled: true,
                              fillColor: Color.fromRGBO(204, 208, 211, 1),
                              labelText: 'Senha'
                          ),
                          validator: (value){
                            if(value == null || value.isEmpty){
                              return "Por favor, digite sua senha";
                            }
                            return null;
                          },
                        ),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            if(_formKey.currentState.validate()) {
                              registerAccoount(
                                  context,
                                  _emailController.text,
                                  _passwordController.text,
                                  _userController.text);
                            }
                          },
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30)
                                  )
                              ),
                              backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(100, 145, 233, 1))
                          ),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                            child: Text(
                                'Cadastre-se',
                                style: TextStyle(
                                    fontSize: 36,
                                    letterSpacing: 3,
                                    fontFamily: 'Pangolin'
                                )
                            ),
                          )
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*1/10),
                    ],
                  ),
                ),
                Container(
                  color: Color.fromRGBO(19, 96, 168, 1),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height*1/12,
                )
              ],
            )
          ],
        )
    );
  }

  void _showErrorMessage(BuildContext context, String title, Exception e) {
    showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(
            title,
            style: const TextStyle(fontSize: 24),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  '${(e as dynamic).message}',
                  style: const TextStyle(fontSize: 18),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                'OK',
                style: TextStyle(color: Colors.deepPurple),
              ),
            ),
          ],
        );
      },
    );
  }

  void registerAccoount(BuildContext context, String email, String password, String username) async{

    try{
      UserCredential credential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password);
      
      await credential.user.updateDisplayName(username);
      var data = {
        'nome_de_usuario': username,
        'id_usuario': FirebaseAuth.instance.currentUser.uid,
        'partidas_jogadas': 0,
        'numero_de_vitorias': 0
      };
      await FirebaseFirestore.instance.collection('usuarios')
          .doc(FirebaseAuth.instance.currentUser.uid)
          .set(data);
    } on FirebaseAuthException catch (e){
      _showErrorMessage(context, "Erro ao cadastrar", e);
    }

  }

}
