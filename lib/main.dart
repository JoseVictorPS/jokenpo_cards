import 'package:flame/util.dart';
import 'package:flutter/material.dart';
import 'package:jokenpo_cards/pages/create_room.dart';
import 'package:jokenpo_cards/pages/history.dart';
import 'package:jokenpo_cards/pages/home.dart';
import 'package:jokenpo_cards/pages/home_logado.dart';
import 'package:jokenpo_cards/pages/initapp.dart';
import 'package:jokenpo_cards/pages/login.dart';
import 'package:jokenpo_cards/pages/multi_player.dart';
import 'package:jokenpo_cards/pages/register.dart';
import 'package:jokenpo_cards/pages/search_room.dart';
import 'package:jokenpo_cards/pages/single_player.dart';
import 'package:jokenpo_cards/pages/test_game.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'globals.dart' as globals;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  globals.navigatorKey = GlobalKey<NavigatorState>();
  globals.loginLock = false;
  globals.notificationLock = false;
  
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: globals.navigatorKey,
      initialRoute: '/init_app',
      routes: {
        '/init_app': (context) => InitApp(),
        '/home': (context) => Home(),
        '/test_game': (context) => MyGame(),
        '/login_page': (context) => LoginPage(),
        '/home_logado': (context) => HomeLogado(),
        '/register_page': (context) => RegisterPage(),
        '/single_player': (context) => SinglePlayer(),
        '/multi_player': (context) => MultiPlayer(),
        '/create_room': (context) => CreateRoom(),
        '/search_room': (context) => SearchRoom(),
        '/history': (context) => History()
      }));

  Util flameUtil = Util();
  flameUtil.fullScreen();
  flameUtil.setOrientation(DeviceOrientation.portraitUp);


  // lida com os toques na tela do jogo
  // TestGame game = TestGame();

  // TapGestureRecognizer tapper = TapGestureRecognizer();
  // tapper.onTapDown = game.onTapDown;
  // flameUtil.addGestureRecognizer(tapper);


}
