import 'dart:ui';
import 'package:flame/sprite.dart';
import 'package:jokenpo_cards/pages/test_game.dart';

class Background {
  final TestGame game;
  Sprite bgSprite;
  Rect bgRect;

  Background(this.game) {
    bgSprite = Sprite('background.png');
    bgRect = Rect.fromLTWH(
      0,
      0,
      1480,
      2760,
    );
  }

  void render(Canvas c) {
    bgSprite.renderRect(c, bgRect);
  }

  void update(double dt) {}
}