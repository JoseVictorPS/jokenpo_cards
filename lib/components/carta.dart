import 'dart:ui';
import 'package:firebase_database/firebase_database.dart';
import 'package:jokenpo_cards/classes/player.dart';
import 'package:jokenpo_cards/pages/test_game.dart';
import 'package:flame/sprite.dart';

class Carta {
  TestGame game; // classe principal do jogo
  Rect cardRect; // retangulo da carta
  bool isPlayed = false; // verifica se a carta está em jogo
  double cardX;
  double cardY;
  Sprite cardSprite;
  Player _player;
  String dbKey;
  int tipoCarta; // 0 = Pedra, 1 = Papel, 2 = Tesoura;
  
  final DatabaseReference db =
      FirebaseDatabase().reference().child('cards/').push();
  bool enemyCard = false;

  Carta(this.game, double x, double y, int tipoCarta) {
    cardX = x;
    cardY = y;
    this.tipoCarta = tipoCarta;
    cardRect = Rect.fromLTWH(cardX, cardY, game.tileSizeX, game.tileSizeY);
    switch (tipoCarta) {
      case 0:
        cardSprite = Sprite('ROCK.png');
        break;
      case 1:
        cardSprite = Sprite('PAPER.png');
        break;
      case 2:
        cardSprite = Sprite('SCISSOR.png');
        break;
    }
  }

  Map<String, dynamic> toJson() {
    return {'player': this._player.dbKey,'dbKey': this.dbKey, 'type': this.tipoCarta, 'left': cardRect.left, "top" : cardRect.top, "width" : cardRect.width, "heigth": cardRect.height};
  }

  Future<DatabaseReference> savePlayer() async {
    this.db.set(this.toJson());
    this.dbKey = this.db.key;
    return this.db;
  }

  void render(Canvas c) {
    cardSprite.renderRect(c, cardRect);
  }

  void update(double dt) {}

  void isNotEnemy() {
    cardRect = Rect.fromLTWH(
        game.screenSize.width / 2 - game.tileSizeX,
        game.screenSize.height / 2 - game.tileSizeY,
        game.tileSizeX * 3,
        game.tileSizeY * 2);
  }

  void onTapDown() {
    if (game.cardOnGame == false) {
      game.cardOnGame = true;
      isPlayed = !isPlayed;
      cardRect = Rect.fromLTWH(
          game.screenSize.width / 2 - game.tileSizeX,
          game.screenSize.height / 2 - game.tileSizeY,
          game.tileSizeX * 3,
          game.tileSizeY * 2);
    }
  }

  Player get player => this._player;

  set setPlayer(Player player) {
    this._player = player;
    this.savePlayer();
  }

  void isEnemyCard() {
    cardRect = Rect.fromLTWH(
        game.screenSize.width / 2 - game.tileSizeX,
        (game.screenSize.height / 2 - 3.08 * game.tileSizeY),
        game.tileSizeX * 3,
        game.tileSizeY * 2);
    this.enemyCard = true;
  }
}
