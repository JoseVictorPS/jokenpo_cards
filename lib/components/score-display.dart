import 'dart:ui';
import 'package:flutter/painting.dart';
import 'package:jokenpo_cards/pages/test_game.dart';

class ScoreDisplay {
  final TestGame game;
  TextPainter painter;
  TextStyle textStyle;
  Offset position;
  double positionx;
  double positiony;
  int score = 0;
  String text;

  ScoreDisplay(this.game, positionx, positiony, text) {
    this.positionx = positionx;
    this.positiony = positiony;
    this.text = text;

    painter = TextPainter(
      textAlign: TextAlign.left,
      textDirection: TextDirection.ltr,
    );

    textStyle = TextStyle(
      color: Color(0xffffffff),
      fontSize: 30,
      shadows: <Shadow>[
        Shadow(
          blurRadius: 7,
          color: Color(0xff000000),
          offset: Offset(3, 3),
        ),
      ],
    );

    position = Offset.zero;
  }

  void render(Canvas c) {
    painter.paint(c, position);
  }

  void update(double t) {
    painter.text = TextSpan(
      text: text + score.toString(), // MUDAR PARA SCORE DO PLAYER AO INVES DE VARIAVEL LOCAL
      style: textStyle,
    );

    painter.layout();

    position = Offset(
      positionx,
      positiony,
    );
  }
}