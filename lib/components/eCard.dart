import 'dart:ui';
import 'package:jokenpo_cards/classes/player.dart';
import 'package:jokenpo_cards/pages/test_game.dart';
import 'package:flame/sprite.dart';


class ECard {
  TestGame game; // classe principal do jogo
  Rect cardRect; // retangulo da carta
  double cardX;
  double cardY;
  Sprite cardSprite;
  Player player;
  int tipoCarta; // 0 = Pedra, 1 = Papel, 2 = Tesoura, 3 = aguardando;
  

  ECard (this.game, int tipoCarta){
    this.tipoCarta = tipoCarta;
    cardRect = Rect.fromLTWH(game.screenSize.width/2 - game.tileSizeX, (game.screenSize.height/2 - 3.08*game.tileSizeY), game.tileSizeX*3, game.tileSizeY*2);
    cardSprite = Sprite('ROCK_E.png');
    switch(tipoCarta){
      case 0:
        cardSprite = Sprite('ROCK_E.png');
        break;
      case 1:
        cardSprite = Sprite('PAPER_E.png');
        break;
      case 2:
        cardSprite = Sprite('SCISSOR_E.png');
        break;
    }
  }

  void render (Canvas c){

    if(tipoCarta < 3) {
      cardSprite.renderRect(c, cardRect);
    }
  }

  void update (double dt){

  }
}