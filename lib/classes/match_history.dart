import 'package:flutter/material.dart';

class MatchHistory {
  String matchName;
  bool won;
  List participants;

  MatchHistory(String matchName, bool won, List participants) {

    this.matchName = matchName;
    this.won = won;
    this.participants = participants;

  }

  List ifWon(){
    if(this.won) {
      return ["Vitória", Colors.lightGreenAccent];
    }
    else {
      return ["Derrota", Colors.red];
    }
  }

}