class ChatMsg {
  String msg;
  String username;
  int userId;
  int id;

  ChatMsg(msg, username){
    this.msg = msg;
    this.username = username;
  }
}