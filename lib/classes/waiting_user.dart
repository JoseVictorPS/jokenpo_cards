import 'package:jokenpo_cards/classes/user.dart';

class WaitingUser extends User {
  bool ready;

  WaitingUser(nome, id, ready) : super(nome, id){
    this.ready = ready;
  }

}