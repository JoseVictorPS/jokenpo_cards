import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:jokenpo_cards/classes/player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jokenpo_cards/components/carta.dart';

class Match {
  Player _host;
  List<Player> players;
  //cartas na mesa
  List<Carta> _cardsPlayed = [];
  // ignore: unused_field
  String _dbKey;
  bool _ongoing = true;
  final DatabaseReference _db =
      FirebaseDatabase().reference().child('matches/').push();
  bool singlePlayer;
  List<Carta> cardsInGame = [];
  Player currentPlayer;

  Map<dynamic, dynamic> toJson() {
    return {
      'host': this._host.toJson(),
      'players': this.playerSetToJson(),
      "ongoing": this._ongoing,
      'currentPlayer': this.getCurrentPlayerJson(),
      'cardsPlayed': this.cardsToJson()
    };
  }

  List cardsToJson(){
    List cards = [];
    for(Carta card in this._cardsPlayed) cards.add(card.toJson());
    return cards;
  }

  updateCurrentPlayer(Player player) async {
    this.currentPlayer = player;
    await this._db.update({"currentPlayer": this.getCurrentPlayerJson()});
  }

  Player get host => this._host;

  bool get ongoing => this._ongoing;

  set ongoing(bool value) => this._ongoing = value;

  set host(Player value) => this._host = value;

  getCurrentPlayerJson() {
    if (this.currentPlayer != null) return this.currentPlayer.toJson();
    return null;
  }

  get db {
    return this._db;
  }

  List<Carta> get cardsPlayed => this._cardsPlayed;

  List<dynamic> playerSetToJson() {
    List<dynamic> players = [];
    this.players.forEach((player) => {players.add(player.toJson())});
    return players;
  }

  Map getPlayersForHistory() {
    Map participants = {};
    this
        .players
        .forEach((player) => {participants[player.name] = player.points});
    return participants;
  }

  Match(List<Player> players, Player host, List<Carta> cardsPlayed,
      bool singlePlayer) {
    this.players = players;
    this._host = host;
    this._cardsPlayed = cardsPlayed;
    this.singlePlayer = singlePlayer;
    this.saveMatch();
  }

  saveMatch() {
    this._db.set(this.toJson());
    this._dbKey = db.key;
    return db;
  }

  updatePlayerOfRound(Player currentPlayer) async {
    this.currentPlayer = currentPlayer;
    await this._db.update(this.getCurrentPlayerJson());
  }

  void addCardPlayed(Carta card) {
    this._cardsPlayed.add(card);
    this.saveMatch();
  }

  //finaliza jogo e cria histórico
  void endMatch() {
    User currentUser = FirebaseAuth.instance.currentUser;
    FirebaseFirestore matchHistory = FirebaseFirestore.instance;

    Map<String, dynamic> history = {
      "nome_da_partida": "Partida do " + this._host.name,
      "participantes": this.getPlayersForHistory(),
      "timestamp": FieldValue.serverTimestamp(),
      "vitoria": this.hasUserWon()
    };

    matchHistory
        .collection("usuarios")
        .doc(currentUser.uid)
        .collection("HistoricoDePartidas")
        .doc(this._dbKey)
        .set(history);

    this._ongoing = false;

    this.updateMatchStatus();
    // Minha ideia era deletar o match, os jogadores e as cartas do jogo, já que elas não serão utilizadas, mas não implementei
    // this._db.remove();
    // this.deletePlayers();
  }

  void updateMatchStatus() {
    this._db.update({"ongoing": this._ongoing});
  }

  void deletePlayers() {
    for (Player player in this.players) {
      player.delete();
    }
  }

  bool hasUserWon() {
    var currentUser = FirebaseAuth.instance.currentUser;

    Player winner = this.players.first;
    for (Player player in this.players) {
      if (player.points > winner.points) {
        winner = player;
      }
    }

    if (winner is HumanPlayer) {
      HumanPlayer humanWinner = winner;
      return humanWinner.userId == currentUser.uid;
    }

    return false;
  }

  // verifica pontuação
  Player checkPontuation() {
    Carta card1 = this._cardsPlayed.elementAt(0);
    Carta card2 = this._cardsPlayed.elementAt(1);

    Player winnerOfRound;

    if (card1.tipoCarta == 1 && card2.tipoCarta == 0) {
      card1.player.updatedPoints();
      winnerOfRound = card1.player;
    } else if (card2.tipoCarta == 1 && card1.tipoCarta == 0) {
      card2.player.updatedPoints();
      winnerOfRound = card2.player;
    } else if (card1.tipoCarta == 2 && card2.tipoCarta == 0) {
      card2.player.updatedPoints();
      winnerOfRound = card2.player;
    } else if (card2.tipoCarta == 2 && card1.tipoCarta == 0) {
      card1.player.updatedPoints();
      winnerOfRound = card1.player;
    } else if (card1.tipoCarta == 1 && card2.tipoCarta == 2) {
      card2.player.updatedPoints();
      winnerOfRound = card2.player;
    } else if (card2.tipoCarta == 1 && card1.tipoCarta == 2) {
      card1.player.updatedPoints();
      winnerOfRound = card1.player;
    }

    this._cardsPlayed = [];

    this.saveMatch();

    this.cardsInGame.remove(card1);
    this.cardsInGame.remove(card2);

    return winnerOfRound;
  }

}
