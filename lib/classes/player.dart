import 'package:firebase_database/firebase_database.dart';
import 'package:jokenpo_cards/components/carta.dart';

abstract class Player {
  bool isPlaying = false;
  int points = 0;
  String userId;
  String dbKey;
  List<Carta> _cardsInHand = [];
  final DatabaseReference db =
      FirebaseDatabase().reference().child('players/').push();
  String name;

  Future<DatabaseReference> savePlayer() async {
    this.db.set(this.toJson());
    this.dbKey = this.db.key;
    return this.db;
  }

  get cardsInHand => this._cardsInHand;

  set cardsInHand(List<Carta> cards) => this._cardsInHand = cards;

  void addCardToHand(Carta carta) {
    this._cardsInHand.add(carta);
    this.updateCardsInHand();
  }

  updatedPoints() {
    this.points++;
    db.update({"points": this.points});
  }

  List cardsToJson() {
    List cards = [];
    for (Carta card in this._cardsInHand) {
      cards.add(card.toJson());
    }
    return cards;
  }

  updateCardsInHand() {
    db.update({"cards": this.cardsToJson()});
  }

  Map<String, dynamic> toJson();

  delete() {
    this.db.remove();
  }
}

class BotPlayer extends Player {
  BotPlayer(String name) {
    this.name = name;
    this.savePlayer();
  }

  // AI do bot
  Carta playCard(List<Carta> cards) {
    if (this._cardsInHand.length == 1) {
      Carta chosenCard = this._cardsInHand.first;
      this._cardsInHand.remove(this._cardsInHand.first);
      this.updateCardsInHand();
      return chosenCard;
    }

    List amountOfCardsPerType = [
      {"qtd": 0, 'type': "rock"},
      {"qtd": 0, 'type': "paper"},
      {"qtd": 0, 'type': "scissor"}
    ];

    int nRock = 0;
    int nPaper = 0;
    int nScissor = 0;

    for (Carta card in cards) {
      if (card.tipoCarta == 1) nPaper++;
      if (card.tipoCarta == 0) nRock++;
      if (card.tipoCarta == 2) nScissor++;
    }

    for (Map elem in amountOfCardsPerType) {
      if (elem['type'] == "rock")
        elem["qtd"] = nRock;
      else if (elem['type'] == "paper")
        elem["qtd"] = nPaper;
      else if (elem['type'] == "scissor") elem["qtd"] = nScissor;
    }

    amountOfCardsPerType.sort((a, b) => b["qtd"].compareTo(a["qtd"]));

    for (var type in amountOfCardsPerType) {
      for (Carta card in this._cardsInHand) {
        Carta cardChosen;
        // tesoura corta papel
        if (card.tipoCarta == 2 && type['type'] == "paper")
          cardChosen = card;
        // papel embrulha pedra
        else if (card.tipoCarta == 1 && type['type'] == "rock")
          cardChosen = card;
        // pedra esmaga tesoura
        else if (card.tipoCarta == 0 && type['type'] == "scissor")
          cardChosen = card;

        if (cardChosen == null) {
          cardChosen = card;
        }

        this._cardsInHand.remove(this._cardsInHand.first);
        this.updateCardsInHand();
        return cardChosen;
      }
    }
    print("não retournou nenhuma carta");
  }

  Map<String, dynamic> toJson() {
    return {
      'isPlaying': this.isPlaying,
      'points': this.points,
      'name': this.name,
      'isBot': true,
      'dbKey': this.dbKey,
      "cards": this.cardsToJson()
    };
  }
}

class HumanPlayer extends Player {
  HumanPlayer(String name) {
    this.name = name;
    this.savePlayer();
  }

  HumanPlayer.withUser(String uid, String name) {
    this.userId = uid;
    this.name = name;
    this.savePlayer();
  }

  Map<String, dynamic> toJson() {
    return {
      'isPlaying': this.isPlaying,
      'points': this.points,
      'isBot': false,
      'name': this.name,
      "userId": this.userId,
      'dbKey': this.dbKey,
      "cards": this.cardsToJson()
    };
  }
}
