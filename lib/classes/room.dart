class Room {
  String nome;
  String hostId;
  String creatorId;
  List<int> playersId;
  String password;
  int nOfPlayers;

  Room(String nome, int nOfPlayers, String password, String hostId, String creatorId) {
    this.nome = nome;
    this.nOfPlayers = nOfPlayers;
    this.password = password;
    this.hostId = hostId;
    this.creatorId = creatorId;
  }

}