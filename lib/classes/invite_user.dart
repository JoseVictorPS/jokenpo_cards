import 'package:jokenpo_cards/classes/user.dart';

class InviteUser extends User {
  String oneSignalId;

  InviteUser(nome, id, oneSignalId) : super(nome, id){
    this.oneSignalId = oneSignalId;
  }

}